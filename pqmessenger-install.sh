#!/bin/bash
# pqMessenger installation tool
# v2.1
# (C) 2019 Abdelhamid Meddeb <abdelhamid@meddeb.net>
#
VERSION=2.1.0
JARFILE=pqmessenger-$VERSION.jar
BOOTFILE=pqmessenger.sh
LOGCONFFILE=pqmessenger.conf
SRVCFILE=pqmessenger.service
MANFILE=pqmessenger.3
MANDIR=/usr/local/share/man
LOG4JFILE=log4j2.xml
CONFFILE=config.properties
KEYSTOREFILE=pqmessenger.p12
CONFFILESDIR=sys-resources
INSTALLDIR=/opt/pqmessenger
LOGDIR=/var/log/pqmessenger
LOGCONFDIR=/etc/rsyslog.d
SRVCDIR=/etc/systemd/system
SYSUSER=openldap
PARAMDIR=
LIBDIR=
JMSLOGIN=
JMSPWD=
UNINSTALL=1
JMSMODIFIED=1
SED=$(command -v sed)

showHeader() {
  echo ""
  echo "----------------------------------------------"
  echo "pqMessenger version $VERSION installation tool"
	echo "Copyright (C) 2019 abdelhamid@meddeb.net"
  echo "----------------------------------------------"
}

showUsage() {
  echo ""
  echo "Usage: $1"
  echo "          Install pqMessenger"
  echo "Or"
  echo "       $1 --uninstall"
  echo "          Uninstall pqMessenger"
  echo "Or"
  echo "       $1 --help|-h"
  echo "          Show this help message"
  echo ""
}

checkUser() {
  USERID=$(id -u)
  if [ $USERID -ne 0 ]; then
    echo "Error, must be run as root user"
    echo ""
    exit 1
  fi
}

checkArg() {
  if [ $2 -gt 1 ]; then
    showUsage $1
    exit 1
  fi
	if [[ "$3" == "-h" || "$3" == "--help" ]]; then
    showUsage $1
    exit 0
  fi
  if [ "$3" == "--uninstall" ]; then
    UNINSTALL=0
    return
  fi
  if [ ! -z $3 ]; then
    showUsage $1
    exit 1
  fi
}

checkFiles() {
  local PWD=$(pwd)
	PWD=$(echo "$PWD" | $SED 's/\(.*\)\/$/\1/g')
  local RSLT=0
  local FILENAME=$(find $PWD -name "$JARFILE" 2>/dev/null | grep -v tmp | head -1)
  if [ -z $FILENAME ]; then
    echo "$JARFILE not found."
    RSLT=1
    else
    JARFILE=$FILENAME
  fi
	local LOCLANG=$(locale | grep LANG= | awk -F "=" '{print $2}' | cut -c1-2)
	if [ "$LOCLANG" == "fr" ]; then
		MANDIR=$MANDIR/fr/man3
		FILENAME=$(find $PWD/doc/fr -name "$MANFILE" 2>/dev/null | head -1)
		else
		MANDIR=$MANDIR/man3
		FILENAME=$(find $PWD/doc -name "$MANFILE" | grep -v fr 2>/dev/null | head -1)
  fi
  if [ -z $FILENAME ]; then
    echo "$MANFILE not found."
    else
	  MANFILE=$FILENAME
	fi
  PWD=$PWD/$CONFFILESDIR
  FILENAME=$(find $PWD -name "$BOOTFILE" 2>/dev/null | head -1)
  if [ -z $FILENAME ]; then
    echo "$BOOTFILE not found."
    RSLT=1
    else
    BOOTFILE=$FILENAME
  fi
  FILENAME=$(find $PWD -name "$LOGCONFFILE" 2>/dev/null | head -1)
  if [ -z $FILENAME ]; then
    echo "$LOGCONFFILE not found."
    RSLT=1
    else
    LOGCONFFILE=$FILENAME
  fi
  FILENAME=$(find $PWD -name "$LOG4JFILE" 2>/dev/null | head -1)
  if [ -z $FILENAME ]; then
    echo "$LOG4JFILE not found."
    RSLT=1
    else
    LOG4JFILE=$FILENAME
  fi
  FILENAME=$(find $PWD -name "$CONFFILE" 2>/dev/null | head -1)
  if [ -z $FILENAME ]; then
    echo "$CONFFILE not found."
    RSLT=1
    else
    CONFFILE=$FILENAME
  fi
  FILENAME=$(find $PWD -name "$SRVCFILE" 2>/dev/null | head -1)
  if [ -z $FILENAME ]; then
    echo "$SRVCFILE not found."
    RSLT=1
    else
    SRVCFILE=$FILENAME
  fi
  return $RSLT
}

checkOSInstall() {
	if [ -f /etc/ldap/pqchecker/pqparams.dat ]; then
			PARAMDIR=/etc/ldap/pqchecker
		elif [ -f /etc/openldap/pqchecker/pqparams.dat ]; then
			PARAMDIR=/etc/openldap/pqchecker
		else
      local VALRET=$(find /etc -mindepth 2 -type f -name 'pqparams.dat')
	    if [ -z $VALRET ]; then
		    echo "Look like pqChecker not installed yet."
		    return 1
	    fi
	    PARAMDIR=$(dirname $VALRET)
	    PARAMDIR=$(echo "$PARAMDIR" | $SED 's/\(.*\)\/$/\1/g')
	fi
  SYSUSER=$(stat -c "%U" $PARAMDIR 2>/dev/null)
	if [ -f /usr/lib/ldap/pqchecker.so ]; then
			LIBDIR=/usr/lib/ldap
		elif [ -f /usr/lib64/openldap/pqchecker.so ]; then
			LIBDIR=/usr/lib64/openldap
		else
      VALRET=$(find /usr -mindepth 2 -maxdepth 4 -name 'pqchecker.so')
	    if [ -z $VALRET ]; then
		    echo "Look like pqChecker not installed yet."
		    return 1
	    fi
	    LIBDIR=$(dirname $VALRET)
	    LIBDIR=$(echo "$LIBDIR" | $SED 's/\(.*\)\/$/\1/g')
	fi
	VALRET=$(command -v systemctl)
	if [ -z $VALRET ]; then
		echo " Look like Systemd not used on target system."
		return 1
	fi
	if [ ! -d $SRVCDIR ]; then
		mkdir -p $SRVCDIR
	fi
  return 0
}

disableBoot() {
	local JFILE=$(basename $JARFILE)	
  local RUNNING=$( ps -ef | grep "$JFILE" | grep -v grep | grep -v root | awk '{print $2}' )
  CMD=$(command -v systemctl)
  if [ -n "$RUNNING" ]; then
    $CMD stop pqmessenger
  fi
  $CMD disable pqmessenger
  rm -f $LOGCONFDIR/$LOGCONFFILE
  rm -f $SRVCDIR/$SRVCFILE
  $CMD restart rsyslog
}

uninstall() {
  echo "Uninstallation.."
  disableBoot
  rm -rf $INSTALLDIR
  CMD=$(command -v find)
  local DIRNAME=$($CMD /etc -name "pqparams.dat")
  if [ ! -z $DIRNAME ]; then
    DIRNAME=$(dirname $DIRNAME)
    if [ -d $DIRNAME ]; then
      rm -f $DIRNAME/$CONFFILE
      rm -f $DIRNAME/$LOG4JFILE
      rm -f $DIRNAME/$KEYSTOREFILE
    fi
  fi
  rm -rf $LOGDIR
  rm -f $MANDIR/man3/$MANFILE
  rm -f $MANDIR/fr/man3/$MANFILE
  echo "Uninstallation done."
}

createKeystore() {
  echo "Keystore install .."
  local CMD=$(command -v keytool)
  local KSFILE=$PARAMDIR/$KEYSTOREFILE
  if [ -z "$CMD" ]; then
    echo "Error! keytool not found"
    return
  fi
  $CMD -genkey -alias pqmessenger -keyalg RSA \
			 -keystore $KSFILE -dname "CN=pqMessenger, OU=pqChecker, O=PPolicy, L=LDAPPPolicy, S=IDF, C=FR" \
			 -keysize 1024 -validity 365 -storepass $JMSPWD -keypass $JMSPWD -storetype pkcs12 2>&1>/dev/null
  if [ $? -eq 0 ]; then 
    echo "done."
    echo "File created: $KSFILE"
    echo "+-------------------------------------------------------------------------------+"
    echo "|                                SECURITY WARNING!                              |"
    echo "+-------------------------------------------------------------------------------+"
    echo "| A certificate is generated within the pqmessenger.p12 file, settings:         |"
    echo "| - Key size: 1024                                                              |"
    echo "| - Validity: 365 days (must be renewed before expiration)                      |"
    echo "| You may recreate it by hand (using the Java 'keytool' tool or using the       |"
    echo "| 'pqmessenger-createkeystore.sh' script but THE SAME KEYSTORE MUST BE USED IN  |"
    echo "| THE JMS SERVER SIDE. So, you must copy it to the configuration location of    |"
    echo "| this server.                                                                  |"
    echo "+-------------------------------------------------------------------------------+"   
    else
    echo "An eror occured while generating keystore, try do this manually."
  fi
}

readPassword() {
  local PWDLEN=0
  while [ "$PWD1" == "" ] || [ "$PWD1" != "$PWD2" ] || [ $PWDLEN -lt 6 ]
  do 
    echo -ne "Password <$JMSPWD>: "
    read -s PWD1
		echo
    echo -ne "Confirm : "
    read -s PWD2
		echo
    if [ -z $PWD1 ] && [ -z $PWD2 ]; then
      PWD1=$JMSPWD
      PWD2=$JMSPWD
    fi
    PWDLEN=${#PWD2}
    if [ "$PWD1" == "" ]; then
      echo "Password cannot empty!"
    elif [ "$PWD1" != "$PWD2" ]; then
      echo "Password confirmation error, reedit !"
    elif [ $PWDLEN -lt 6 ]; then
      echo "Password size must be greater than 5 characters"  
    fi
  done
  if [ "$LOGIN" != "$JMSLOGIN" ]; then
    JMSMODIFIED=0
  fi
  JMSPWD=$PWD1
}

setJMSConfig() {
  echo "Communication with the JMS server settings:"
	local CFILE=$PARAMDIR/$(basename $CONFFILE)	
  JMSLOGIN=$(grep login $CFILE | awk -F '=' '{ print $2 }' | $SED "s/ //g")
  JMSPWD=$( grep password $CFILE | awk -F '=' '{ print $2 }' | $SED "s/ //g")
  local LOGIN=""
  while [ "$LOGIN" == "" ]
  do 
    LOGIN=$JMSLOGIN
    echo -ne "Login <$JMSLOGIN>: "
    read LOGIN
    if [ -z $LOGIN ]; then
      echo "The login cannot be empty, reenter !"
    fi
  done
  if [ "$LOGIN" != "$JMSLOGIN" ]; then
    JMSMODIFIED=0
  fi
  JMSLOGIN=$LOGIN
  readPassword
  if [ $JMSMODIFIED -eq 0 ]; then
    echo "Default credentials modified, modiying settings in $CFILE .."
    $SED -i "s/^login.*/login = $JMSLOGIN/g" $CFILE
    $SED -i "s/^password.*/password = $JMSPWD/g" $CFILE
    echo "Modification done."
  fi
  createKeystore
}

setAccessMode() {
	local JFILE=$INSTALLDIR/$(basename $JARFILE)	
	local BFILE=$INSTALLDIR/$(basename $BOOTFILE)	
	local CFILE=$PARAMDIR/$(basename $CONFFILE)	
	local LCFILE=$PARAMDIR/$(basename $LOG4JFILE)	
	local LSFILE=$LOGCONFDIR/$(basename $LOGCONFFILE)	
	local SFILE=$SRVCDIR/$(basename $SRVCFILE)	
  local KSFILE=$PARAMDIR/$KEYSTOREFILE
  chown root:root $JFILE
	chmod 644 $JFILE
  chown root:root $BFILE
	chmod 755 $BFILE
  chown root:root $SFILE
	chmod 644 $SFILE
  chown root:root $LSFILE
	chmod 644 $LSFILE
  chown $SYSUSER:$SYSUSER -R $LOGDIR
	chmod 750 $LOGDIR
  chown $SYSUSER:$SYSUSER $LCFILE
  chown $SYSUSER:$SYSUSER $CFILE
	if [ -f $KSFILE ]; then
    chown $SYSUSER:$SYSUSER $KSFILE
	  chmod 600 $KSFILE
	fi
}

customizeParams() {
	setJMSConfig
	local CFILE=$PARAMDIR/$(basename $CONFFILE)	
	local BFILE=$INSTALLDIR/$(basename $BOOTFILE)	
	local SFILE=$SRVCDIR/$(basename $SRVCFILE)	
  $SED -i "s/^User.*/User=$SYSUSER/g" $SFILE
  $SED -i "s/^Group.*/Group=$SYSUSER/g" $SFILE
	# to escape slash in path folders
	local BFILE=$(echo "$BFILE" | $SED -e "s/\//\\\\\//g")
	local PFDIR=$(echo "$PARAMDIR" | $SED -e "s/\//\\\\\//g")
	local LFDIR=$(echo "$LIBDIR" | $SED -e "s/\//\\\\\//g")
	local GFDIR=$(echo "$LOGDIR" | $SED -e "s/\//\\\\\//g")
  $SED -i "s/^ExecStart.*/ExecStart=$BFILE start -c $PFDIR/g" $SFILE
  $SED -i "s/^ExecStop.*/ExecStop=$BFILE stop/g" $SFILE
  $SED -i "s/^keystore.*/keystore = $KEYSTOREFILE/g" $CFILE
  $SED -i "s/^nativelibpath.*/nativelibpath = $LFDIR/g" $CFILE
  $SED -i "s/^logpath.*/logpath = $GFDIR/g" $CFILE
}

install() {
  echo "Installation.."
  checkFiles
  local RSLT=$?
  if [ $RSLT -ne 0 ]; then
    echo "Error, configuration file(s) not found."
    echo ""
    exit 1
  fi
  checkOSInstall
  RSLT=$?
  if [ $RSLT -ne 0 ]; then
    echo "Error, required installation not found."
    echo ""
    exit 1
  fi
  mkdir -p $INSTALLDIR
  mkdir -p $LOGDIR
  cp -p $JARFILE $INSTALLDIR
  cp -p $BOOTFILE $INSTALLDIR
  cp -p $LOGCONFFILE $LOGCONFDIR
  cp -p $LOG4JFILE $PARAMDIR
  cp -p $CONFFILE $PARAMDIR
  cp -p $SRVCFILE $SRVCDIR
	if [ ! -z $MANFILE ]; then
    mkdir -p $MANDIR
    cp -p $MANFILE $MANDIR
	fi
  customizeParams
  setAccessMode
  CMD=$(command -v systemctl)
  $CMD enable pqmessenger
  $CMD restart rsyslog
  $CMD start pqmessenger
  echo "Installation done."
}

## main
showHeader
checkUser
checkArg $0 $# $1
if [ $UNINSTALL -eq 0 ]; then
  uninstall
  else
  install
fi  
echo ""
