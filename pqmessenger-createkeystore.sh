#!/bin/bash
#
# Keystore creator tool for pqMessenger 
# (C) 2017-2019 Abdelhamid MEDDEB <abdelhamid@meddeb.net>

KSPWD=
KSCFILE=pqmessenger.p12
TSCFILE=pqmessenger-trust.p12
KSSFILE=msgserver.p12
TSSFILE=msgserver-trust.p12

readPassword() {
  local PWD1=""
  local PWD2=""
  local PWDLEN=0
  while [[ "$PWD1" == "" || "$PWD1" != "$PWD2" || $PWDLEN -lt 6 ]]
  do 
    echo -ne "Enter keystore password: "
    read -s PWD1
    echo
    echo -ne "Confirm: "
    read -s PWD2
    echo
    PWDLEN=${#PWD1}
    if [ "$PWD1" == "" ]; then
      echo "Password cannot be empty!"
			echo ""
    elif [ "$PWD1" != "$PWD2" ]; then
      echo "Password confirmation error !"
			echo ""
    elif [ $PWDLEN -lt 6 ]; then
      echo "Password size must be greater than 5 characters"  
			echo ""
    fi
  done
  KSPWD=$PWD1
}

opensslGen() {
		openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -subj "/C=FR/ST=Denial/L=Springfield/O=Dis/CN=www.example.com" -keyout certif.key  -out certif.crt
		openssl pkcs12 -export -in certif.crt -inkey certif.key -out certif.p12 -name pqmessenger -password pass:pqmessenger
}

keytoolGen() {

}


echo "----------------------------------------"
echo "Keystore generation tool for pqMessenger"
echo "----------------------------------------"
CMD=$(command -v keytool)
if [ -z "$CMD" ]; then
  echo "Error! keytool not found"
  exit 1
fi
echo "----------------------------------------"
echo "Keystore generation tool for pqMessenger"
echo "----------------------------------------"
if [ -f $KSFILE ]; then
	R="N"
	echo -ne "A keystore file $KSFILE already exists, overwrite ? (N/y) "
	read R
	echo
	if [[ "$R" != "Y" && "$R" != "y" ]]; then
		echo "Keystore creation aborted."
		echo ""
		exit 0
	fi
	rm -f $KSFILE
fi
readPassword
echo "keystore creation.."
$CMD -genkey -alias pqmessenger -keyalg RSA -keystore $KSFILE \
		 -dname "CN=pqMessenger, OU=pqChecker, O=PPolicy, L=LDAPPPolicy, S=IDF, C=FR" \
		 -keysize 1024 -validity 365 -storepass $KSPWD -keypass $KSPWD -storetype pkcs12 2>&1>/dev/null
if [ $? -eq 0 ]; then 
  echo "done."
  echo "Keystore file $KSFILE successfully created."
  else
  echo "An eror occured"
fi
echo ""
