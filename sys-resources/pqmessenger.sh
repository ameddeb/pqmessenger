#!/bin/bash
# pqMessenger starting shell script
# (c) 2015-2019 Abdelhamid MEDDEB <abdelhamid@meddeb.net>

# return pqMessegner starting aguments
pqmessengerARGS() {
  ARGS=""
  if [ ! -z $CONFIG_HOME ]; then
    ARGS="--config-path $CONFIG_HOME"
  fi
}

# set pqMessenger FQN JAR file
setPqmessengerFQN() {
	local FIND=$(command -v find)
	if [ ! -z $FIND ]; then
		NB=$($FIND $PQMESSENGER_HOME -maxdepth 1 -type f -readable -name '*.jar' 2>/dev/null | wc -l)
		if [ $NB -eq 0 ]; then
			echo "pqMessenger binary JAR file not found"
		  elif [ $NB -gt 1 ]; then
			  echo "There are several binary JAR files of pqMessenger"
		  else
				PQMESSENGER_FULLBIN=$($FIND $PQMESSENGER_HOME -maxdepth 1 -type f -readable -name '*.jar')
		fi
	fi
}

# Check Java installation & required version
checkJava() {
  local SED=$(command -v sed)
  local JBIN=$(command -v java)
  if [ -z $JBIN ]; then
    echo "Java not installed."
    exit 1
  fi
  local VERSION_WORD=$($JBIN -version 2>&1 | grep -i version | awk '{for (i=1;i<=NF;i++){if ($i=="version") {print $(i+1)}}}')
  local JVERSION=$(echo $VERSION_WORD | $SED 's/[^0-9\.]*//g')
  if [ $(echo $JVERSION | cut -c1-2) == "1." ]; then
    JVERSION=$(echo $JVERSION | awk -F "." '{print $2}')
    else  
    JVERSION=$(echo $JVERSION | awk -F "." '{print $1}')
  fi
  if [ $JVERSION -lt 8 ]; then
    echo "pqMessenger require Java 1.8 or above."
    exit 1
  fi  
}

# Checks the mandatory elements for pqMessenger execution
checkMandatory() {
	if [ ! -d $CONFIG_HOME ]; then
		echo "The configuration folder $CONFIG_HOME doesn't exists"
		exit 1
	fi
  local SED=$(command -v sed)
  CONFIG_HOME=$(echo "$CONFIG_HOME" | $SED 's/\(.*\)\/$/\1/g' | $SED 's/ //g')
	local CONFIG_FQN=$CONFIG_HOME/$CONFIG_FILE
	if [ ! -r $CONFIG_FQN ]; then
		echo "The configuration file $CONFIG_HOME/$CONFIG_FILE doesn't exists or readable"
		exit 1
	fi
	local VAL=$(grep nativelibpath $CONFIG_FQN | head -1 | awk -F '=' '{print $2}')
	if [ ! -z $VAL ]; then
		NATIVE_LIB_HOME=$VAL
	fi
  NATIVE_LIB_HOME=$(echo "$NATIVE_LIB_HOME" | $SED 's/\(.*\)\/$/\1/g' | $SED 's/ //g')
  local NATIVELIB_FQN=$NATIVE_LIB_HOME/$NATIVE_LIB_FILE
  if [ ! -r $NATIVELIB_FQN ]; then
    echo "The pqChecker native library $NATIVELIB_FQN doesn't exists or readable"
    exit 1
  fi
	VAL=$(grep logpath $CONFIG_FQN | head -1 | awk -F '=' '{print $2}')
	if [ ! -z $VAL ]; then
		LOG_HOME=$VAL
	fi
  LOG_HOME=$(echo "$LOG_HOME" | $SED 's/\(.*\)\/$/\1/g' | $SED 's/ //g')
  if [ ! -w $LOG_HOME ]; then
    echo "The log folder $LOG_HOME doesn't exists or writable"
    exit 1
  fi
}

# pid function, return pqMessenger pid
pqmessengerPid() {
  PID=$( ps -ef | grep "$PQMESSENGER_FULLBIN" | grep -v grep | grep -v root | awk '{print $2}' )
  echo $PID
}

# start pqMessenger
do_start(){
	checkJava
	checkMandatory
  DAEMONPID=$( pqmessengerPid )
  if [ -z "$DAEMONPID" ]; then
		echo -n "Starting pqMessenger... "
		local JBIN=$(command -v java)
    JAVA_ARGS="-Djava.library.path=$NATIVE_LIB_HOME"
    OUTFILE="$LOG_HOME/pqmessenger.out"
    PQMESSENGER_ARGS="--config-path $CONFIG_HOME"
    $JBIN $JAVA_ARGS -jar $PQMESSENGER_FULLBIN $PQMESSENGER_ARGS >> $OUTFILE 2>&1
    RETVAL=$?
    DAEMONPID=$( pqmessengerPid )
    if [[ $RETVAL -eq 0  &&  ! -z $DAEMONPID ]]; then
      echo "Ok."
    else
			echo "Error."
			RETVAL=1
    fi 
  else
    echo "Already running"
    RETVAL=0
  fi       
}

# stop pqMessenger
do_stop(){
  DAEMONPID=$( pqmessengerPid )
  if [ ! -z $DAEMONPID ]; then
		echo -n "Stopping pqMessenger... "
		kill $DAEMONPID	
    RETVAL=$?
    if [ $RETVAL -eq 0 ]; then
      echo "Ok."
    else
      echo "Error."
    fi
  else      
    echo "Not running"
    RETVAL=0
  fi
}

setParameters() {
	while [ "$1" != "" ]; do
		case $1 in
			start | stop)
					CMD=$1
					;;
			-c | --config-home)
					shift
					CONFIG_HOME=$1
					;;
			-l | --log-home)
					shift
					LOG_HOME=$1
					;;
			-n | --nativelib-home)
					shift
					NATIVE_LIB_HOME=$1
					;;
			* )
					echo "Launch parameters error"
					exit 1
		esac
		shift
  done
}
	
#--------- main --------
# Default parameters values
PQMESSENGER_HOME=$(dirname $0)
NATIVE_LIB_HOME=/usr/lib/ldap
CONFIG_HOME=/etc/ldap/pqchecker
LOG_HOME=/var/log/pqmessenger
CONFIG_FILE=config.properties
CONFIGLOG_FILE=log4j2.xml
CMD=
setParameters $@
NATIVE_LIB_FILE=pqchecker.so
PQMESSENGER_FULLBIN=
setPqmessengerFQN
RETVAL=0
case "$CMD" in
  start)
    do_start;
    ;;
  stop)
    do_stop
    ;;
	* )
		echo "Launch parameters error"
		exit 1
		;;
esac
