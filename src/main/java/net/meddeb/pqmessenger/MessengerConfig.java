package net.meddeb.pqmessenger;
/*--------------------------------------------------------------------
pqMessenger, a messaging middleware for pqChecker.
Copyright (C) 2015-2019, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

import java.util.List;

public class MessengerConfig {
	public static class Server {
		private String host;
		private int port;
		public String getHost() {
			return host;
		}
		public void setHost(String host) {
			this.host = host;
		}
		public int getPort() {
			return port;
		}
		public void setPort(int port) {
			this.port = port;
		}
	}
	private String login;
	private String password;
	private boolean servicediscovery;
	private int cnxretrytime;
	private String keystorefile;
	private String keystorepwd;
	private String truststorefile;
	private String truststorepwd;
	private String nativelibpath;
	private String logpath;
	private List<Server> servers;
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isServicediscovery() {
		return servicediscovery;
	}
	public void setServicediscovery(boolean servicediscovery) {
		this.servicediscovery = servicediscovery;
	}
	public int getCnxretrytime() {
		return cnxretrytime;
	}
	public void setCnxretrytime(int cnxretrytime) {
		this.cnxretrytime = cnxretrytime;
	}
	public String getKeystorefile() {
		return keystorefile;
	}
	public void setKeystorefile(String keystorefile) {
		this.keystorefile = keystorefile;
	}
	public String getKeystorepwd() {
		return keystorepwd;
	}
	public void setKeystorepwd(String keystorepwd) {
		this.keystorepwd = keystorepwd;
	}
	public String getTruststorefile() {
		return truststorefile;
	}
	public void setTruststorefile(String truststorefile) {
		this.truststorefile = truststorefile;
	}
	public String getTruststorepwd() {
		return truststorepwd;
	}
	public void setTruststorepwd(String truststorepwd) {
		this.truststorepwd = truststorepwd;
	}
	public String getNativelibpath() {
		return nativelibpath;
	}
	public void setNativelibpath(String nativelibpath) {
		this.nativelibpath = nativelibpath;
	}
	public String getLogpath() {
		return logpath;
	}
	public void setLogpath(String logpath) {
		this.logpath = logpath;
	}
	public List<Server> getServers() {
		return servers;
	}
	public void setServers(List<Server> servers) {
		this.servers = servers;
	}
}
