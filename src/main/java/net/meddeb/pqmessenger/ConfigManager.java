package net.meddeb.pqmessenger;
/*--------------------------------------------------------------------
pqMessenger, a messaging middleware for pqChecker.
Copyright (C) 2015-2019, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

public class ConfigManager {
	public final static String VERSION = "2.1.0";

	private final static String CONFPATH_ARGKEY = "--config-path";
	private final static String CONFFILE_ARGKEY = "--config-file";
	private final static String CONFLOG_ARGKEY = "--config-log";
	
	private final static String DEFAULT_CONFPATH = "/etc/ldap/pqchecker";
	private final static String DEFAULT_CONFFILE = "pqmessenger.yml";
	private final static String DEFAULT_LOGFILE = "log4j2.xml";
	private final static int DEFAULT_CNXRETRY_TIME = 600; //10mn

	private MessengerConfig messengerConfig = null;
	
	private Logger logger = null;
	
	private void logWelcomeMessage(){
		if (logger == null) return;
		logger.info("");
		logger.info(LoggingMsg.getOut("sepLine"));
		String msg = MessageFormat.format(LoggingMsg.getOut("pqmsgTitle"), 
				LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")), ConfigManager.VERSION);
		logger.info(msg);
		logger.info("Copyright (C) 2015-2019, Abdelhamid MEDDEB (abdelhamid@meddeb.net)");
		logger.info(LoggingMsg.getOut("freeNoWarranty"));
		logger.info(LoggingMsg.getOut("sepLine"));
		logger.info("");
	}
	
	
	private String addTrailingSeparator(String path){
		String rslt = path;
		String pathSeparator = System.getProperty("file.separator");
		if (pathSeparator.isEmpty()) pathSeparator = "/";
		if (path.charAt(path.length()-1) != pathSeparator.charAt(0)){
			rslt = rslt + pathSeparator;
		}
		return rslt;
	}
	
	/**
	 * @param String[] args: of initialization arguments
	 * @param String argKey: argument key
	 * @return String argument value, empty is no argument found
	 */
	private String getArgValue(String[] args, String argKey){
		String rslt = "";
		if (args.length > 1){
			for (int i=0; i<args.length; i++){
				if ((args[i].equals(argKey)) && (i<(args.length-1))){
					rslt = args[i+1];
					break;
				}
			}
		}
		return rslt;
	}

	private boolean initializeLog(String fqnConfiglogFile) {
		boolean rslt = Files.exists(Paths.get(fqnConfiglogFile), LinkOption.NOFOLLOW_LINKS);
		if (rslt) {
			LoggerContext context = (LoggerContext) LogManager.getContext(false);
	    File file = new File(fqnConfiglogFile);
	    context.setConfigLocation(file.toURI());
	  	logger = LogManager.getLogger(ConfigManager.class);
	  	if (logger == null) rslt = false;
	  	else logWelcomeMessage();
		}
    return rslt;
	}
	
	private void initialize(String configPath, String configFile) {
		InputStream inputStream = null;
		try {
			Yaml yaml = new Yaml(new Constructor(MessengerConfig.class));			
			inputStream = new FileInputStream(configPath + configFile);
			messengerConfig = yaml.load(inputStream);
		} catch (IOException e) {
			logger.error(LoggingMsg.getLog("confFileErr"));
		}
	}
	
	private void initialize(String[] args) {
		String configPath = getArgValue(args, CONFPATH_ARGKEY);
		if (configPath.isEmpty()) configPath = DEFAULT_CONFPATH;
		configPath = addTrailingSeparator(configPath);
		String configFile = getArgValue(args, CONFLOG_ARGKEY);
		if (configFile.isEmpty()) configFile = DEFAULT_LOGFILE;
		if (!initializeLog(configPath + configFile)) {
			System.err.println(MessageFormat.format(LoggingMsg.getLog("cantConfLog"), configPath + configFile));
		}
		
		configFile = getArgValue(args, CONFFILE_ARGKEY);
		if (configFile.isEmpty()) configFile = DEFAULT_CONFFILE;
		initialize(configPath, configFile);
	}
	public ConfigManager(String[] args) {
		initialize(args);
	}

	public MessengerConfig getMessengerConfig() {
		return messengerConfig;
	}
	
	public boolean isInitialized() {
		return (messengerConfig != null) && (messengerConfig.getServers() != null) &&
				(messengerConfig.getServers().size()>0);
	}
	
	public int getCnxretrytime() {
		if (messengerConfig == null || messengerConfig.getCnxretrytime()<=0) return DEFAULT_CNXRETRY_TIME;
		else return messengerConfig.getCnxretrytime();
	}
	
}
