package net.meddeb.pqmessenger;
/*--------------------------------------------------------------------
pqMessenger, a messaging middleware for pqChecker.
Copyright (C) 2015-2019, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.util.HashMap;

import javax.jms.Connection;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import net.meddeb.pqcommon.PwdChannelProperties;
import net.meddeb.pqcommon.PwdChannelMsg;
import net.meddeb.pqcommon.PwdProcessStatus;
import net.meddeb.pqcommon.PwdTestChannelMsg;

import org.apache.activemq.artemis.api.core.TransportConfiguration;
import org.apache.activemq.artemis.api.jms.ActiveMQJMSClient;
import org.apache.activemq.artemis.api.jms.JMSFactoryType;
import org.apache.activemq.artemis.core.remoting.impl.netty.NettyConnectorFactory;
import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Messenger {
	private static Messenger instance = null;
	private String senderID = "";
	private Logger logger = LogManager.getLogger(this.getClass());
	private Connection connection = null;
	private Topic topic = null;
	private boolean connected = false;
	private boolean connectionInitialized = false;
	//listen fields
	private Session session = null;
	private MessageConsumer consumer = null;
	
	private String getHostname(){
		String rslt = "";
		try {
			rslt = InetAddress.getLocalHost().getCanonicalHostName();
		} catch (UnknownHostException e) {
			rslt="";
		} finally{
			if (rslt == null) rslt = "";
		}
		return rslt;
	}
	
	private class ParamsListener implements MessageListener{
		public void onMessage(Message message) {
			logger.debug(LoggingMsg.getLog("receivMsg"));
			TextMessage msg = null;
			try {
				if (message instanceof TextMessage) {
					msg = (TextMessage) message;
					String msgType = msg.getStringProperty(PwdChannelProperties.TYPE.strName());
					if ((msgType == null) || msgType.isEmpty()){
						logger.warn(LoggingMsg.getLog("msgNonUsable"));
						return;
					}
					logger.debug(MessageFormat.format(LoggingMsg.getLog("msgReceived"), msgType, msg.getText()));
					String params = "";
					switch (PwdChannelMsg.value(msgType)){
						case WRITE_REQUEST:
							params = msg.getText();
							if (JNIGateway.getInstance().setParams(params.trim(), PwdChannelProperties.QSFORMAT.strName())){
								logger.info(MessageFormat.format(LoggingMsg.getLog("paramsModif"), msg.getText()));
								doSend(PwdProcessStatus.SUCCESS.strName(), PwdChannelMsg.WRITE_RESPONSE.strName());
							} else doSend(PwdProcessStatus.FAIL.strName(), PwdChannelMsg.WRITE_RESPONSE.strName());
							break;
						case READ_REQUEST:
							params = JNIGateway.getInstance().getParams(PwdChannelProperties.QSFORMAT.strName());
							if (params == null){
								doSend(PwdProcessStatus.FAIL.strName(), PwdChannelMsg.READ_RESPONSE.strName());
							} else doSend(params.trim(), PwdChannelMsg.READ_RESPONSE.strName());
							break;
						default: //when response message, do nothing but test
							if (PwdTestChannelMsg.value(msgType) == PwdTestChannelMsg.PWDTEST_REQUEST) {
								doSend(PwdProcessStatus.SUCCESS.strName(), PwdTestChannelMsg.PWDTEST_RESPONSE.strName());
							}
							break;	
					}
				} else {
					logger.warn(LoggingMsg.getLog("typeNotsupp") + message.getClass().getName());
				}
			} catch (JMSException e) {
				logger.error(LoggingMsg.getLog("msgError") + e.toString());
			} catch (Exception e) {
				logger.error(LoggingMsg.getLog("sysError") + e.toString());
			}
		}
	}
	
	private Messenger() {
	}
	
	public static Messenger getInstance() {
		if (instance == null) instance = new Messenger();
		return instance;
	}

	public boolean initConnection(MessengerConfig messengerConfig) {
		if (connectionInitialized) return true;
		if (messengerConfig == null || messengerConfig.getServers() == null || messengerConfig.getServers().size() == 0) {
			connectionInitialized = false;
			return false;
		}
		this.senderID = getHostname();
		connectionInitialized = false;
		logger.debug(LoggingMsg.getLog("initCnx"));
		HashMap<String, Object> map = null;
		TransportConfiguration[] transportConfigList = new TransportConfiguration[messengerConfig.getServers().size()];
		try {
			int idx = 0;
			for (MessengerConfig.Server server: messengerConfig.getServers()) {
			  map =new HashMap<String, Object>();
				map.put("host", server.getHost());
				map.put("port", server.getPort());
				map.put("sslEnabled", true);
				if (messengerConfig.getTruststorefile() != null && !messengerConfig.getTruststorefile().isEmpty()) {
					map.put("trustStorePath", messengerConfig.getTruststorefile());
					map.put("trustStorePassword", messengerConfig.getTruststorepwd());
				}
				if (messengerConfig.getKeystorefile() != null && !messengerConfig.getKeystorefile().isEmpty()) {
					map.put("keyStorePath", messengerConfig.getKeystorefile());
					map.put("keyStorePassword", messengerConfig.getKeystorepwd());
				}
				transportConfigList[idx] = new TransportConfiguration(NettyConnectorFactory.class.getName(), map);
				idx++;
			}
			
			ActiveMQConnectionFactory connectionFactory = ActiveMQJMSClient.createConnectionFactoryWithHA(JMSFactoryType.CF, transportConfigList);
			connectionFactory.setReconnectAttempts(2);
			connectionFactory.setUseTopologyForLoadBalancing(true);
			connectionFactory.setFailoverOnInitialConnection(true);
			connectionFactory.setCallFailoverTimeout(100);
			logger.debug(LoggingMsg.getLog("factoryInit"));
			connection = connectionFactory.createConnection(messengerConfig.getLogin(), messengerConfig.getPassword());
			
			if (connection != null) {
				connection.setExceptionListener(new ExceptionListener() {
					public void onException(JMSException e) {
						connected = false;
						JNIGateway.getInstance().setCacheData(true);
						connectionInitialized = false;
						logger.warn(LoggingMsg.getLog("cnxLost"));
					}
				});
			}
			connectionInitialized = (connection != null);
			if (connectionInitialized) logger.debug(LoggingMsg.getLog("cnxSuccess"));
			else logger.debug(LoggingMsg.getLog("cnxUnable"));
		} catch (Exception e) {
			logger.error(LoggingMsg.getLog("cnxUnable"));
		}
		return connectionInitialized;
		
	}
	
	public boolean startConnection(){
		logger.debug(LoggingMsg.getLog("cnxStart"));
		connected = false;
		if (!connectionInitialized) return connected;
		String selectCondition = "(" + 
				PwdChannelProperties.TYPE.strName() + " = '" + PwdChannelMsg.READ_REQUEST.strName() + "' OR " + 
				PwdChannelProperties.TYPE.strName() + " = '" + PwdChannelMsg.WRITE_REQUEST.strName() + "' OR " +
				PwdChannelProperties.TYPE.strName() + " = '" + PwdTestChannelMsg.PWDTEST_REQUEST.strName() + "') AND " +
				"(" + PwdChannelProperties.SENDERID.strName() + " <> '" + senderID + "')";
		logger.debug(LoggingMsg.getLog("listenSel") + selectCondition);
		try {
			connection.start();
			session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
			topic = session.createTopic(PwdChannelProperties.CHANNELID.strName());
			consumer = session.createConsumer(topic, selectCondition);
			consumer.setMessageListener(new ParamsListener());
			connected = true;
			logger.debug(LoggingMsg.getLog("cnxStartSucc"));
		} catch (JMSException e) {
			logger.error(LoggingMsg.getLog("cnxStartErr") + e.getMessage());
		}
		return connected;
	}
	
	public void stopConnection(){
		try{
			if ((connected) && (connection != null)) {
				logger.debug(LoggingMsg.getLog("cnxStop"));
				connection.stop();
				if (session != null) {
					session.close();
					session = null;
				}
				consumer = null;
				connected = false;
			}
		} catch (JMSException e) {
			logger.error("cnxStopErr" + e.getMessage());
		}
	}

	public void doSend(String message, String msgType){
		if ((connection == null) || (!connectionInitialized) || (session == null)) return;
		logger.debug(LoggingMsg.getLog("sendMsg"));
		try{
			MessageProducer producer = session.createProducer(topic);
			TextMessage msg  = session.createTextMessage();
			msg.setStringProperty(PwdChannelProperties.TYPE.strName(), msgType);
			msg.setStringProperty(PwdChannelProperties.SENDERID.strName(), senderID);
			msg.setText(message);
			logger.debug(MessageFormat.format(LoggingMsg.getLog("msgSending"), msgType, message));
			producer.send(msg);
			logger.debug(LoggingMsg.getLog("msgSent"));
		} catch (JMSException e){
			logger.error(e.getMessage());
		}
	}
	
	public boolean isConnected() {
		return connected;
	}

	public boolean isConnectionInitialized() {
		return connectionInitialized;
	}

}
