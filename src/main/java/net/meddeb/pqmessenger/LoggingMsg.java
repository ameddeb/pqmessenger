package net.meddeb.pqmessenger;
/*--------------------------------------------------------------------
pqMessenger, a messaging middleware for pqChecker.
Copyright (C) 2015-2019, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

import java.util.ResourceBundle;

class LoggingMsg {
	private static final String LOG_PROPNAME = "net.meddeb.pqmessenger.pqmsglogmsg";
	private static final String OUT_PROPNAME = "net.meddeb.pqmessenger.pqmsgoutmsg";
	private static ResourceBundle logMessages = null;
	private static ResourceBundle outMessages = null;
	private static ResourceBundle getLogMessages(){
		if (logMessages == null) logMessages = ResourceBundle.getBundle(LOG_PROPNAME);
		return logMessages;
	}
	private static ResourceBundle getOutMessages(){
		if (outMessages == null) outMessages = ResourceBundle.getBundle(OUT_PROPNAME);
		return outMessages;
	}
	
	public static String getLog(String key){
		String msg = getLogMessages().getString(key);
		return msg;
	}
	public static String getOut(String key){
		String msg = getOutMessages().getString(key);
		return msg;
	}
	private LoggingMsg(){
	}
}
