package net.meddeb.pqmessenger;
/*--------------------------------------------------------------------
pqMessenger, a messaging middleware for pqChecker.
Copyright (C) 2015-2019, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MsgDaemon extends Thread {
	private boolean stopped = false;
	private boolean connected = false;
	private long startWait = 0;
	private MsgEngine msgEngine = null;
	private Logger logger = null;
	public MsgDaemon() {
	}

	public void init(String[] args) {
		connected = false;
		msgEngine = new MsgEngine(args);
		logger = LogManager.getLogger(this.getClass());
	}

	@Override
  public void run() {
		if (!msgEngine.isConfigDone()) {
	    System.err.println(LoggingMsg.getOut("wrongConf"));
	    return;
		}
    if (!msgEngine.doListen()) logger.error(LoggingMsg.getLog("dataCommPb"));
    while(!stopped){
			logger.info(LoggingMsg.getLog("pqMsgTrycnx"));
			msgEngine.startConnection();
			if (msgEngine.isConnected()){
				connected = true;
			} else {
				String msg = LoggingMsg.getLog("pqMsgCnxFail");
				msg = MessageFormat.format(msg, msgEngine.getStrTimeretry());
				logger.info(msg);
			}
			while (!stopped && msgEngine.isConnected()){
				try{
					Thread.sleep(100);
				} catch (InterruptedException e) {
					logger.error("Error: " + e.getMessage());
				}
			}
			if (connected){
				logger.info(LoggingMsg.getLog("pqMsgdcnx"));
				connected = false;
			}
      //--wait
			startWait  = System.currentTimeMillis();
			while (!stopped && (System.currentTimeMillis() - startWait) < msgEngine.getTimeRetry()) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					logger.error("Error: " + e.getMessage());
				} 
			} //wait
		}
    System.out.println(LoggingMsg.getOut("sepLine"));
		String msg = MessageFormat.format(LoggingMsg.getOut("pqMsgStop"), 
				LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
    System.out.println(msg);
    System.out.println(LoggingMsg.getOut("sepLine"));
  }
	
	public void doStop() {
		this.stopped = true;
	}
}
