package net.meddeb.pqmessenger;
/*--------------------------------------------------------------------
pqMessenger, a messaging middleware for pqChecker.
Copyright (C) 2015-2019, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/


import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Valid arguments:
 * 1/ --config-path						:	path to configuration files (log and conf), default /etc/ldap/pqchecker 
 * 2/ --config-file						:	configuration file name, default config.properties 
 * 3/ --config-log						:	logging configuration file name, default log4j2.xml
 * 4/ --connection-retry-time	: time, in seconds, to retry when connection to messaging server fail or lost.
 */
public class MsgEngine {
	private ConfigManager configManager = null;
	private Thread listener = null;
	private Logger logger = null;

	
	private void printWelcomeMessage(){
		System.out.println("");
		System.out.println(LoggingMsg.getOut("sepLine"));
		String msg = MessageFormat.format(LoggingMsg.getOut("pqmsgTitle"), 
				LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")), ConfigManager.VERSION);
		System.out.println(msg);
		System.out.println("Copyright (C) 2015-2019, Abdelhamid MEDDEB (abdelhamid@meddeb.net)");
		System.out.println(LoggingMsg.getOut("freeNoWarranty"));
		System.out.println(LoggingMsg.getOut("sepLine"));
		System.out.println("");
	}

	public MsgEngine(String[] args) {
		printWelcomeMessage();
		configManager = new ConfigManager(args);
		logger =  LogManager.getLogger(this.getClass());
	}
	
	public void startConnection(){
		Messenger.getInstance().initConnection(configManager.getMessengerConfig());
		if (Messenger.getInstance().isConnectionInitialized()) {
			Messenger.getInstance().startConnection();
	   	JNIGateway.getInstance().setCacheData(!Messenger.getInstance().isConnected());
			if ((logger != null)&&(Messenger.getInstance().isConnected())) {
				logger.info(LoggingMsg.getLog("pqMsgcnx"));
			}
		}
	}
	
	public void stopConnection(){
		Messenger.getInstance().stopConnection();
   	JNIGateway.getInstance().setCacheData(!Messenger.getInstance().isConnected());
		if ((logger != null)&&(!Messenger.getInstance().isConnected()))	{
			logger.info(LoggingMsg.getLog("pqMsgdcnx"));
		}
	}
	
	/**
	 * Connection retry time in milliseconds
	 * @return time to retry cnx 
	 */
	public long getTimeRetry() {
		return configManager.getCnxretrytime() * 1000;
	}
	/**
	 * Return human readable retry time
	 * @return String, readable time
	 */
	public String getStrTimeretry() {
		int t = configManager.getCnxretrytime();
		String rslt = "";
		if (t < 60) {
			rslt = Integer.toString(t) + " s";
		} else if (t < 3600) {
			int s = t % 60;
			t = t / 60;
			rslt = Integer.toString(t) + " mn";
			if (s > 0) rslt = rslt + ", " + Integer.toString(s) + " s";
		} else {
			int s = t % 3600;
			t = t / 3600;
			s = s % 60;
			int m = s / 60;
			rslt = Integer.toString(t) + " h";
			if (m > 0) rslt = rslt + ", " + Integer.toString(m) + " mn";
			if (s > 0) rslt = rslt + ", " + Integer.toString(s) + " s";
		}
		return rslt;
	}

  public boolean doListen() {
    JNIGateway.getInstance().setCacheData(true);
	  listener = new Listener();
	  listener.start();
	  return true;
  }

  public void stopListen() {
    JNIGateway.getInstance().stopListen();
  }
  
  public String getVersion() {
  	return ConfigManager.VERSION;
  }
  
  public boolean isConnected() {
  	return Messenger.getInstance().isConnected();  
  }
  
  public boolean isConfigDone() {
  	return configManager.isInitialized();
  }

}
