package net.meddeb.pqmessenger;
/*--------------------------------------------------------------------
pqMessenger, a messaging middleware for pqChecker.
Copyright (C) 2015-2019, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/


public class JNIGateway {
	static {
		try {
			System.loadLibrary("pqchecker");
		} catch (UnsatisfiedLinkError e) {
	    System.err.println(LoggingMsg.getOut("failLoadLibrary"));
	    System.exit(1);
	  }		
	}
  private static JNIGateway instance = null;
	
	private JNIGateway() {
	}

  public static JNIGateway getInstance() {
    if (instance == null) instance = new JNIGateway();
    return instance;
  }

	public native String getParams(String fmt);
	
	public native boolean setParams(String params, String fmt);

	public native void setCacheData(boolean cacheData);

	public native void stopListen();
	
}
