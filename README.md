#### Communication middleware for pqChecker.

Java daemon allows to communicate the passwords strength settings and new/modified passwords itself.

The password quality settings are stored in a text file for pqChecker component. These settings may be modified by a system administrator. But pqChecker allows reading and modifying them, programmatically. It allows, also, broadcasting the modified passwords, in real time, to another systems who use it (database system, mails server..).

pqMessenger ensure the communication of these data between pqChecker component and a MoM that support JMS protocol.

![alt tag](https://www.meddeb.net/pqmessenger/res/pqmessenger-overview.png)

For further details visit https://www.meddeb.net/pqmessenger
